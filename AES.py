from functools import partial
from functools import reduce
import copy
import tables
import operator
from utils import transpose
from SHA3 import SHA3_256

def _split_at(word, n):
    return [word[:n], word[n:]]

def _rot_word(word):
    return word[1:] + [word[0]]

Nr = int(10)
Nk = int(4)

# split list for lists of lengths of n
def _split_every (list, n):
    return [list[i:i+n] for i in range(0,len(list), n)]

def _word_to_states(word):
    bytestr = list(bytearray(word, 'utf-8'))
    while (len(bytestr) % (Nk*4) != 0):
        bytestr += [0x00]
    return list(map(partial(_split_every, n = 4), _split_every(bytestr, Nk*4)))

def _cipher(state, w):
    keys = _split_every(list(map(list, transpose(w))), 4)
    state = _add_round_key(state, keys[0])
    for i in range(1, Nr-1):
        state = _sub_bytes(state)
        state = _shift_rows(list(state))
        state = _mix_columns(list(state))
        state = _add_round_key(state, keys[i])
    state = _sub_bytes(state)
    state = _shift_rows(state)
    state = _add_round_key(state, keys[Nr-1])
    return list(state)

def _inv_cipher(state, w):
    keys = _split_every(list(map(list, transpose(w))), 4)
    keys = keys[::-1]
    state = _add_round_key(state, keys[0])
    for i in range(1, Nr-1):
        state = _inv_shift_rows(list(state))
        state = _sub_bytes(state, tables.inv_sbox)
        state = _add_round_key(state, keys[i])
        state = _mix_columns(list(state), _mix_inverse_matrix)
    state = _inv_shift_rows(state)
    state = _sub_bytes(state, tables.inv_sbox)
    state = _add_round_key(state, keys[Nr-1])
    return list(state)

def _to_str(x):
    if (len(x) < 1):
        return []
    return reduce(operator.add, x)

def _AES_128(word, keyword, f_cipher = _cipher):
    if len(keyword) > 128:
        raise Exception("Maximum key length is 128 bit")
    if(len(keyword) <= 0):
        keyword = [chr(0x00)]
    key = _word_to_states(SHA3_256(keyword))[0]
    states = _word_to_states(word)
    _w = _key_expansion(key)
    c = list(map(partial(f_cipher, w = _w), states))
    ciphers = list(map(lambda x: list(map(list,x)), c))
    coded_bytes = _to_str(list(map(_to_str, ciphers)))
    print(coded_bytes)
    return _to_str(list(map(chr, coded_bytes)))

AES_128_encrypt = _AES_128
AES_128_decrypt = partial(_AES_128, f_cipher = _inv_cipher)

matrix_to_list = lambda x: list(map(list, copy.deepcopy(x)))

def _key_expansion(key):
    key = transpose(key)
    res = matrix_to_list(key)
    i = Nk
    while (i < Nr*Nk):
        if (i % Nk == 0):
            res += [list(map(lambda x,y,z: x ^ y ^ z, _sub_bytes([_rot_word(res[i-1])])[0], res[i-Nk], tables.rcon[int(i/Nk)]))]
        else:
            res += [list(map(operator.xor, res[i-Nk], res[i-1]))]
        i += 1
    return transpose(res)

def _add_round_key(state, key):
    return list(map(partial(map, operator.xor), state, key))

def _sub_bytes(state, r = tables.sbox):
    return list(map(partial(map, r.__getitem__), state))

def _shift_rows(state):
    def shift(i):
        parts = _split_at(list(state[i]), i)
        return parts[1] + parts[0]
    return list(map(shift, range(len(list(state)))))

def _inv_shift_rows(state):
    reverse = lambda x: x[::-1]
    state = list(map(list,(state)))
    res = list(map(reverse, state))
    return list(map(reverse, _shift_rows(res)))

identity = lambda x: x
appl = lambda f,x: f(x)

_mix_matrix = [
    [tables.mult_2.__getitem__, tables.mult_3.__getitem__, identity, identity],
    [identity, tables.mult_2.__getitem__, tables.mult_3.__getitem__, identity],
    [identity, identity, tables.mult_2.__getitem__, tables.mult_3.__getitem__],
    [tables.mult_3.__getitem__, identity, identity, tables.mult_2.__getitem__]
]

_mix_inverse_matrix = [
    [tables.mult_14.__getitem__, tables.mult_11.__getitem__, tables.mult_13.__getitem__, tables.mult_9.__getitem__],
    [tables.mult_9.__getitem__, tables.mult_14.__getitem__, tables.mult_11.__getitem__, tables.mult_13.__getitem__],
    [tables.mult_13.__getitem__, tables.mult_9.__getitem__, tables.mult_14.__getitem__, tables.mult_11.__getitem__],
    [tables.mult_11.__getitem__, tables.mult_13.__getitem__, tables.mult_9.__getitem__, tables.mult_14.__getitem__]
]

def _mix_columns(state, op = _mix_matrix):
    mix_col = lambda col: list(map(partial(reduce,operator.xor), map(lambda x:map(appl, x, col), op)))
    return matrix_to_list(transpose(list(map(mix_col, transpose(state)))))
