from functools import partial

import permutations as _per
import init_data as _init_data
import utils as _utils
import copy as _copy

def _keccak_f(A):
    for i in range(_init_data.n_rounds):
        A = _per.round(A, _init_data.RC[i])
    return A

def _keccak(word, c = 512, d = 256):
    r = int(1600 - c)

    P = _utils.pad_word(word, r/8)
    P = _utils.split_every(P, _init_data.w/8)
    P = list(map(lambda x: _utils.word_to_int(x), P))
    P = _utils.split_every(P, r/_init_data.w)
    P = map(partial(map, _utils.little_endian), P)

    S = [[0]*5 for i in range(_init_data.box_size)]

    for Pil in P:
        Pi = list(Pil)
        for x in range(_init_data.box_size):
            for y in range(_init_data.box_size):
                if ((x + 5*y) < (r/_init_data.w)):
                    S[x][y] ^= Pi[x + 5*y]
        S = _keccak_f(S)

    Z = _utils.to_str(list(map(lambda x: list(map(lambda x: _utils.little_endian(int(x)), x)), _utils.transpose(S))))

    return _utils.to_str(map(lambda x: format(x, '016x'), Z))[:int(d/4)]

SHA3_256 = _keccak
SHA3_512 = partial(_keccak, c = 1024, d = 512)
SHA3_384 = partial(_keccak, c = 768, d = 384)
SHA3_224 = partial(_keccak, c = 448, d = 224)
