from __future__ import print_function
from functools import partial
import operator

_alpha = map(chr, map(lambda x: ord('a') + x,range(0,26)))\
    + list(map(chr, map(lambda x: ord('A') + x,range(0,26))))
_keylen = 26

_to_str = partial(reduce, operator.add)

def _caesar (offset, buf, f):
        if (offset < 0 or offset >= _keylen or len(buf) < 1):
                raise Exception("Bad key: must be between 0 and 26 and text must be not empty")
        transform = lambda x: _alpha[f(_alpha.index(x),offset)%len(_alpha)]
        return _to_str(map(transform, buf))

def brutal (buf, word = ""):
        if(len(buf) < 1):
            raise Exception("Text is empty")
        keys = range(0,_keylen)
        texts = map(partial(caesar_decrypt, buf = buf), keys)
        all_pairs = zip(keys, texts)
        if (word != ""):
                right_key = filter(lambda x: x[1].find(word) >= 0, all_pairs)
                print("Possible right key: ", right_key)
        print("(key, text)")
        map(print, all_pairs)

def _vigener(key, buf, f):
        if(len(key) < 1 or len(buf) < 1):
            raise Exception("Key and text must not be empty")
        extended_key = (key*(len(buf)//len(key)+1))[:len(buf)]
        transform = lambda x,y: _alpha[f(_alpha.index(x), _alpha.index(y)) % len(_alpha)]
        return _to_str(map(transform, buf, extended_key))

def _split (n, list):
        return [list[i:i+n] for i in range(0,len(list), n)]

def _hill (key, buf):
        key = matrix(key)
        if(key.determinant() == 0 or len(buf) < 1):
            raise Exception("Key matrix has no inverse or text is empty")
        keylen = len(key.column(0))
        while (len(buf) % keylen != 0):
                buf+= buf[0:keylen - len(buf)%keylen]
        words = _split(keylen, buf)
        mult = lambda x: matrix(x) * key
        coded_vectors  = map(mult, map(partial(map, _alpha.index), words))
        coded_digits = map(lambda x: map(lambda y: y % len(_alpha), x.row(0)), coded_vectors)
        return _to_str(_to_str(map(partial(map, _alpha.__getitem__),coded_digits)))

# returns (gcd, a^-1 mod n)
def ext_gcd(_n, _a):
    ((u, up, v, vp, n, a),(q, r)) = ((0, 1, 1, 0, _n, _a),(_n//_a, _n%_a))
    while (r != 0):
        (u, up, v, vp, n, a) = (up - q*u, u, vp - q*v, v, a, r)
        (q,r) = (n//a, n%a)
    return ((u*_n + v*_a), v % _n)

def pow_mod(a, t, n):
    b = map(Integer, format(t, 'b'))[::-1]
    f = lambda (x,ai), ti: (((x*ai) % n) if (ti == 1) else x, (ai*ai) % n)
    return reduce(f, b, (1, a))[0]

caesar_decrypt = partial(_caesar, f = operator.sub)
caesar_crypt = partial(_caesar, f = operator.add)

vigener_decrypt = partial(_vigener, f = operator.sub) 
vigener_crypt = partial(_vigener, f = operator.add) 

def hill_crypt(key, buf):
    key = matrix(key)
    if (gcd(key.determinant(), len(_alpha)) != 1):
        raise Exception("Matrix has no inverse mod 52")
    return _hill(key, buf)

hill_decrypt = _hill
