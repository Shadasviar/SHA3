import operator
from functools import reduce

def ascii_to_bin(s):
    return reduce(operator.add, map(lambda x: format(ord(x), "08b"), s))

# converts list of chars to string
def to_str(hm):
    h = list(hm)
    if (len(h) < 1):
        return ""
    return reduce(operator.add, h)

def little_endian(x):
    return int(to_str(to_str(list(reversed(split_every(format(int(x), "064b"), 8))))), 2)

word_to_int = lambda x: int(str(ascii_to_bin(x)), 2)

# split list for lists of lengths of n
def split_every (lst, n):
    return [list(lst)[i:i+int(n)] for i in range(0,len(list(lst)), int(n))]

# in bytes
def pad_word(word, size):
    ext_len = size-len(word)%size
    if (ext_len == 1):
        word += chr(0x86)
        return word
    word += to_str([chr(0x06)] + [chr(0x0)]*int(ext_len-2) + [chr(0x80)]*int(ext_len-abs(ext_len-2)-1))
    return word

def split_at(word, n):
    return [word[:n], word[n:]]

# Bitwise rotation of W with length of w by r bits to left
def rot(W, r, w):
    r = r % w
    b = split_at(format(W, "0"+str(w)+"b"), r)
    return int(b[1] + b[0], base=2)

def transpose(x):
    return list(map(list,zip(*x)))
